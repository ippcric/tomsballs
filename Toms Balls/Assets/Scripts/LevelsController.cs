﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class LevelsController : MonoBehaviour
{

    List<int> levelsingame = new List<int>() { 1, 2, 3 }; //initialise list
    public int selectedscene;

    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            print("space key was pressed");
            SelectLevel();
            LoadNextLevel();
        }

        
    }

    private void LoadNextLevel()  //load level method
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;   //get scene index of current scene. Can be chnged in build seetings
        int nextSceneIndex = currentSceneIndex + 1; //use randomisation or just latin squares

        SceneManager.LoadScene(selectedscene); //todo might need to convert string to int
    }

  
    void SelectLevel()
    {
        if (levelsingame.Count == 0)
        {
            print("all run"); //make this into UI text
            QuitGame(); 

        }
        else
        {
            selectedscene = levelsingame[Random.Range(0, levelsingame.Count)]; //select a random level
            Debug.Log(selectedscene);
            var index = levelsingame.IndexOf(selectedscene);
            levelsingame.RemoveAt(index); //remove the selected scene using its index in the list
        }


    }

    public void QuitGame()
    {
        UnityEditor.EditorApplication.isPlaying = false;   //close the unity editor game player

    }
}
