﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatAndBallCollision : MonoBehaviour {

    public bool batcollision = false;
    public string targetcollision = null;
    public Vector3 _ballpos; 
    private void Start()
    {
        targetcollision = "nothing"; 
    }

    private void FixedUpdate()
    {
        _ballpos = gameObject.transform.position; 
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "bat")
        {
            batcollision = true; 
        }
        if (collision.gameObject.tag == "outer")
        {
            targetcollision = "outer";
        }
        if (collision.gameObject.tag == "middle")
        {
            targetcollision = "middle";
        }
        if (collision.gameObject.tag == "inner")
        {
            targetcollision = "inner";
        }
    }
} //todo tag bits of target 
