﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using UnityEngine;
using UnityEngine.UI; 

namespace Tobii.Research.Unity
{
   
    
        public class DisplayMessage : MonoBehaviour
    {
        // Gaze trail script.
        private VRGazeTrail _gazeTrail;
        private Transform latestHitObject;
        private GameObject testObject;
        private GameObject _button;
        private GameObject _text;
        private GameObject _menu; 
        
        // Use this for initialization
        void Awake()
        {
            
            testObject = GameObject.Find("CubeToFind"); 
            _button = GameObject.Find("Button1");
            _text = GameObject.Find("Text1");
            _menu = GameObject.Find("TextHolder");
            _text.SetActive(false);
            _button.SetActive(false);
           

        }

        // Update is called once per frame
        void Update()
        {
            _gazeTrail = VRGazeTrail.Instance;
            latestHitObject = _gazeTrail.LatestHitObject;

            if (latestHitObject == testObject.transform)
            {
                _text.SetActive(true);
                _button.SetActive(true);
                _menu.SetActive(false); 
            }
           
            
        }
    }
}