﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Force : MonoBehaviour
{
    [SerializeField] float ballreleaseDelay = 2f;  //time to delay ball release

    public float thrust;
    public float upthrust; 
    public Rigidbody rb;
   

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        //rb.constraints = RigidbodyConstraints.FreezePositionX & RigidbodyConstraints.FreezePositionY &
            //RigidbodyConstraints.FreezePositionZ; //freeze the ball position until the force is added
        //rb.AddForce(transform.forward * thrust);
        Invoke("addforce", ballreleaseDelay);
        //rb.AddForce(transform.up * thrust);

    }

    private void addforce()
    {
        rb.constraints = RigidbodyConstraints.None; //unfreeze to add force
        rb.AddForce(0, upthrust, -thrust, ForceMode.Impulse);
    }

    // Update is called once per frame
    void FixedUpdate()
    {

    }
}
