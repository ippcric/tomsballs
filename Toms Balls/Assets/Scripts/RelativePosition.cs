﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RelativePosition : MonoBehaviour
{
    public Transform cam;
    public Vector3 cameraRelative;
    
    void Start()
    {
        cam = GameObject.FindWithTag("MainCamera").transform;
        Vector3 cameraRelative = cam.InverseTransformDirection(transform.position);

        print("Cube is " + transform.position); 
        print("Camera is " + cam.position);
        print("Relative position is " + cameraRelative);
        
    }
}

