﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement; 

namespace Tobii.Research.Unity
{

    public class WriteToText : MonoBehaviour
    {
        private string fileName; 
        private float _time;
        StreamWriter writer;
        private VRGazeTrail _gazeTrail; //initiate gaze trail variable
        private VREyeTracker _eyeTracker; //initiate 
        private GameObject battracker; //the end capsule of the bat to track
        private bool hit;
        private string target;
        private Vector3 ballposition; 
        
        //private var output; //string var for latest hit object
        //and maybe input box for save file name. 

        [SerializeField]
        [Tooltip("Enter participant's name here")]
        private string participant = "David"; //enter participant name in inspector. Default to David. 

        [SerializeField]
        [Tooltip("Enter trial number here")]
        private int trial = 1;

        [SerializeField]
        [Tooltip("If true, data is saved.")]
        private bool _saveData;
        
        void Start()
        {
            _gazeTrail = VRGazeTrail.Instance; //cache prefab scripts
            _eyeTracker = VREyeTracker.Instance;
            battracker = GameObject.FindWithTag("endcapsule");
            int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;

            fileName = participant + "_" + currentSceneIndex + "_" + System.DateTime.Now.ToString("yyyyMMddTHHmmss") + ".csv";
            writer = new StreamWriter(@"Data\" + fileName);   //instantiate streamwriter instance
            writer.WriteLine("Seconds" + "," + "Gaze.Location" + "," + "CombinedGazeRay" + "," +
               "," + "Head x" + "," + "Head y" + "," + "Head z" + "," +
               "Bat x" + "," + "Bat y" + "," + "Bat z" + "," +
               "Ball x" + "," + "Ball y" + "," + "Ball z" + "," + 
               "Ball released" + "," + "Bat and ball contact" + "," + "Target hit");  //write titles to file
        }

        // Update is called once per frame
        void FixedUpdate() //frequency dependent on project settings 60 or 120Hz, as ET is 120.
        {
                       
            if (Input.GetKeyDown(KeyCode.V)) //toggles save on and off
            { 
                _saveData = !_saveData;
                Debug.Log(_saveData); 
            }

           
            if (_saveData == true)
            {
                var data = _eyeTracker.LatestGazeData; //gat gaze data
                var ray = data.CombinedGazeRayWorld; //get gaze ray in world from gaze data
                var batpos = battracker.transform.position; //get world position of bat

                //get batcollision boolean 
                hit = GameObject.Find("Ball").GetComponent<BatAndBallCollision>().batcollision;
                //get target collision
                target = GameObject.Find("Ball").GetComponent<BatAndBallCollision>().targetcollision;
                //get the ball position transform
                ballposition = GameObject.Find("Ball").GetComponent<BatAndBallCollision>()._ballpos; 

                _time = Time.time; //time since play
                writer.WriteLine(_time.ToString("#.000") + ", " + "_gazeTrail.LatestHitObject" + "," +
                    "ray.ToString()" + "," + "HEADPOSITION" + "," + batpos.ToString() + "," + ballposition + "," + 
                    target.ToString() + "," + hit.ToString() + ",");  // #.000 limits digits after decimal

                Debug.Log("writing....."); 

                //todo print trial number; and ball release time;; and ball hit time
                //separate summary file: time of ball release, time of hit, if target hit (and which circle). 
            }
                        
        }

        private void OnDestroy()
        {

            writer.Close(); //close the stream to make sure it writes.
        }

    }
}

